
module.exports = {
  dbUrl: 'mongodb://localhost/music',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  }
};
