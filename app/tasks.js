const express= require('express');
const auth = require('../middlewere/auth');
const Task = require('../models/Task');

const createRouter = ()=>{
    const router = express.Router();

    router.get('/', auth, (req, res) =>{
        Task.find()
            .then(result => res.send(result))
            .catch(() => res.sendStatus(500));
    });

    router.post('/',auth, (req,res)=>{

        const task = new Task({
            title: req.body.title,
            description: req.body.description,
            user: req.user._id,
            status: req.body.status
        });

         task.save()
             .then((result )=>res.send(result))
             .catch(()=>res.sendStatus(400))
    });
    router.put('/:id', auth, async (req,res)=>{
        if(req.body.user){ delete req.body.user }

        const editTask = req.body;

        const changeTask = await Task.findById({_id: req.params.id, user: req.user._id});
        changeTask.title = editTask.title;
        changeTask.description = editTask.description;
        changeTask.status = editTask.status;

        await changeTask.save()
            .then((result )=>res.send(result))
            .catch(()=>res.sendStatus(400))
    });

    router.delete('/:id', auth, async (req,res)=>{
        await Task.remove({_id: req.params.id, user: req.user._id});
         res.send({message: "OK"})

    });

    return router;
};

module.exports = createRouter;